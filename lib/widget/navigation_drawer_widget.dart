import 'package:flutter/material.dart';
import 'package:prycegas_approver/Login.dart';
import 'package:prycegas_approver/PETTY.dart';
import 'package:prycegas_approver/detail/CA.dart';
import 'package:prycegas_approver/detail/PO.dart';
import 'package:prycegas_approver/Userpage.dart';
import 'package:prycegas_approver/detail/PR.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NavigationDrawerWidget extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<NavigationDrawerWidget> {
 String urlImage = "" ;

  final padding = EdgeInsets.symmetric(horizontal: 20);

  SharedPreferences? preferences;
  String username = '';
  String fullname = '';
  String position = '';
  String id = '';


  @override
  void initState() {
    getdata();

  }


  logout() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('id');

  }


  getdata() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username  = prefs.getString('position') ?? '';
      fullname  = prefs.getString('fullname') ?? '';
      position  = prefs.getString('position') ?? '';
      id  = prefs.getString('id') ?? '';
    });
    if(id=="1"){
      urlImage = 'http://prycegas.net/pomobile/img/rdr.jpg';
    }else if(id=="2"){
      urlImage = 'http://prycegas.net/pomobile/img/ragas.jpg';
    }else if(id=="3"){
      urlImage = 'http://prycegas.net/pomobile/img/reynamz.jpg';
    }else if(id=="4"){
      urlImage = 'http://prycegas.net/pomobile/img/abillar.jpg';
    }else if(id=="5"){
      urlImage = 'http://prycegas.net/pomobile/img/camay.jpg';
    }

  }

  @override
  Widget build(BuildContext context) {

    return Drawer(
      child: Material(
        color: Colors.white,
        child: ListView(
          children: <Widget>[
            buildHeader(
              urlImage: urlImage,
              name: fullname,
              user: username,
              onClicked: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => UserPage(
                  name: fullname,
                  urlImage: urlImage,
                ),
              )),
            ),
            Container(
              padding: padding,
              child: Column(
                children: [

                  const SizedBox(height: 24),
                  buildMenuItem(
                    text: 'GAS PO',
                    icon: Icons.local_gas_station_outlined,
                    onClicked: () => selectedItem(context, 0),
                  ),
                  const SizedBox(height: 16),
                  buildMenuItem(
                    text: 'Purchase Requisition',
                    icon: Icons.card_travel,
                    onClicked: () => selectedItem(context, 1),
                  ),
                  // const SizedBox(height: 16),
                  // buildMenuItem(
                  //   text: 'Petty Cash',
                  //   icon: Icons.money,
                  //   onClicked: () => selectedItem(context, 2),
                  // ),
                  const SizedBox(height: 24),
                  Divider(color: Colors.black38),
                  const SizedBox(height: 16),
                  buildMenuItem(
                    text: 'Log Out',
                    icon: Icons.logout_rounded,
                    onClicked: () => selectedItem(context, 2),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHeader({
    required String urlImage,
    required String name,
    required String user,
    required VoidCallback onClicked,
  }) =>
      InkWell(
        onTap: onClicked,
        child: Container(
          color: Colors.deepOrange,
          padding: padding.add(EdgeInsets.symmetric(vertical: 40)),
          child: Column(
            children: [
              CircleAvatar(radius: 30, backgroundImage: NetworkImage(urlImage)),
              Text(
                name,
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              const SizedBox(height: 4),
              Text(
                user,
                style: TextStyle(fontSize: 14, color: Colors.white),
              ),
            ],
          ),
        ),
      );

  Widget buildSearchField() {
    final color = Colors.white;

    return TextField(
      style: TextStyle(color: color),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        hintText: 'Search',
        hintStyle: TextStyle(color: color),
        prefixIcon: Icon(Icons.search, color: color),
        filled: true,
        fillColor: Colors.white12,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    final color = Colors.orange;
    final hoverColor = Colors.deepOrange;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(text, style: TextStyle(color: color)),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int index) {
    Navigator.of(context).pop();

    switch (index) {
      case 0:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => POPage(),
        ));
        break;
      case 1:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => PRPage(),
        ));
        break;
      // case 2:
      //   Navigator.of(context).push(MaterialPageRoute(
      //     builder: (context) => PETTYPage(),
      //   ));
      //   break;
      case 2:
        logout();
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => LoginPage(),
        ));
        break;
    }
  }
}
