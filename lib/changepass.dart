// @dart=2.9
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:prycegas_approver/Login.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ChangePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<ChangePage> {
  String username = '';
  Map user1;
  TextEditingController olduser = TextEditingController();
  TextEditingController newpass = TextEditingController();
  TextEditingController conpass = TextEditingController();
  SharedPreferences preferences;

  void change() async{
    final response = await http.post(Uri.parse("http://prycegas.net/pomobile/changepass.php"),body:{
      "user": username,
      "oldpass": olduser.text,
      "newpass": newpass.text,
    });

    if(response.statusCode==200){
      final data = jsonDecode(response.body);
      if(data['val']==1){
        print(data['info']);
        user1 = data['info'];
        Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage()),);
        logout();
      }else{
        Fluttertoast.showToast(msg: data['statut'],toastLength: Toast.LENGTH_SHORT);
      }

    }

  }

  logout() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('id');

  }


  getdata() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username  = prefs.getString('user') ?? '';
    });

  }

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    getdata();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Change Password"),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Column(
              children: <Widget>[
                SizedBox(height: 80,),
                Text("Change your password here!"),
                SizedBox(height: 30,),

                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    children: <Widget>[
                      TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                          ),
                          hintText: 'Current Password',
                        ),
                        controller: olduser,
                      ),

                      SizedBox(height: 15.0,),
                      TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                          ),
                          hintText: 'Create New Password',
                        ),
                        controller: newpass,
                      ),

                      SizedBox(height: 15.0,),

                      TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Confirm Password',
                        ),
                        controller: conpass,
                      ),
                      SizedBox(height: 60,),
                      MaterialButton(
                        minWidth: double.infinity,
                        height: 60,
                        onPressed: (){
                          change();
                        },
                        color: Color(0xffF78504),
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),

                        ),
                        child: Text(
                          "Confirm", style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: Colors.white,

                        ),
                        ),

                      ),


                    ],
                  ),
                ),

              ],
            ))
          ],
        ),
      ),
    );
  }
}

// we will be creating a widget for text field
Widget inputFile({label, obscureText = false})
{
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        label,
        style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w400,
            color:Colors.black87
        ),

      ),
      SizedBox(
        height: 5,
      ),
      TextField(
        obscureText: obscureText,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0,
                horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.grey
              ),

            ),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey)
            )
        ),
      ),
      SizedBox(height: 10,)
    ],
  );
}