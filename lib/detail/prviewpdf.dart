// @dart=2.9
import 'dart:async';
import 'dart:convert';
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:prycegas_approver/Main_Screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;


class PRViewPage extends StatefulWidget {

  final String url;
  final String urlpo;
  final String code;
  final String pocode;
  final String request;
  final String recommended;
  final String dept;
  final String purpose;
  final String remarks;
  final String dateCreated;
  final String vendor;
  final String item1;
  final String item2;
  final String item3;
  final String item4;
  final String item5;
  final String item6;
  final String item7;
  final String item8;
  final String item9;
  final String item10;
  final String item11;
  final String item12;
  final String item13;
  final String item14;
  final String item15;
  final String item16;
  final String item17;
  final String item18;
  final String item19;
  final String item20;
  final String item21;
  final String item22;
  final String item23;
  final String item24;
  final String item25;
  final String item26;
  final String item27;
  final String item28;
  final String item29;
  final String item30;
  final String item31;
  final String item32;
  final String item33;
  final String item34;
  final String item35;
  final String item36;
  final String item37;
  final String item38;
  final String item39;
  final String item40;


  PRViewPage({ this.url, this.urlpo, this.code, this.pocode, this.request, this.recommended, this.dept, this.purpose,this.remarks, this.dateCreated, this.vendor, this.item1, this.item2, this.item3, this.item4, this.item5, this.item6, this.item7, this.item8, this.item9, this.item10, this.item11, this.item12, this.item13, this.item14, this.item15, this.item16, this.item17, this.item18, this.item19, this.item20, this.item21, this.item22, this.item23, this.item24, this.item25, this.item26, this.item27, this.item28, this.item29, this.item30, this.item31, this.item32, this.item33, this.item34, this.item35, this.item36, this.item37, this.item38, this.item39, this.item40});

  @override
  State<StatefulWidget> createState() {
    return _ViewPageState();
  }
}


class _ViewPageState extends State<PRViewPage> {
 PdfViewerController _pdfViewerController;
  final GlobalKey<SfPdfViewerState> _pdfViewerStateKey = GlobalKey();

  PdfViewerController Controller1;
  GlobalKey<SfPdfViewerState> StateKey1 = GlobalKey();
  String datetime = DateTime.now().toString();
  SharedPreferences preferences;
  String username = '';
  String fullname = '';
  String position = '';
  String id = '';


  Future _approve() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final response = await http.post(Uri.parse("http://prycegas.net/pomobile/prpoapproved.php"),body:{
      "code":widget.code,
      "pocode":widget.pocode,
      "dateApproved": datetime,
      "fullname": prefs.getString('fullname') ?? '',
      "request": widget.request,
      "recommended": widget.recommended,
      "dept": widget.dept,
      "purpose": widget.purpose,
      "remarks": widget.remarks,
      "dateCreated": widget.dateCreated,
      "vendor": widget.vendor,
      "item1": widget.item1,
      "item2": widget.item2,
      "item3": widget.item3,
      "item4": widget.item4,
      "item5": widget.item5,
      "item6": widget.item6,
      "item7": widget.item7,
      "item8": widget.item8,
      "item9": widget.item9,
      "item10": widget.item10,
      "item11": widget.item11,
      "item12": widget.item12,
      "item13": widget.item13,
      "item14": widget.item14,
      "item15": widget.item15,
      "item16": widget.item16,
      "item17": widget.item17,
      "item18": widget.item18,
      "item19": widget.item19,
      "item20": widget.item20,
      "item21": widget.item21,
      "item22": widget.item22,
      "item23": widget.item23,
      "item24": widget.item24,
      "item25": widget.item25,
      "item26": widget.item26,
      "item27": widget.item27,
      "item28": widget.item28,
      "item29": widget.item29,
      "item30": widget.item30,
      "item31": widget.item31,
      "item32": widget.item32,
      "item33": widget.item33,
      "item34": widget.item34,
      "item35": widget.item35,
      "item36": widget.item36,
      "item37": widget.item37,
      "item38": widget.item38,
      "item39": widget.item39,
      "item40": widget.item40,
      // "user1": widget.user1,
      // "user2": widget.user2,
      // "user3": widget.user3,
      // "user4": widget.user4,

    });

    if(response.statusCode==200){
      final data = jsonDecode(response.body);
      if(data['val'] == 1){
        Fluttertoast.showToast(msg: data['statut'],toastLength: Toast.LENGTH_SHORT);
        // Navigator.of(context, rootNavigator: true).pop('dialog');
      }else{
      }
    }
  }

  Future _reject() async {
    final response = await http.post(Uri.parse("http://prycegas.net/pomobile/reject.php"),body:{
      "code":widget.code,
      "status": "Rejected",
    });
    Navigator.push(context, MaterialPageRoute(builder: (context)=>MainPage()),);
    if(response.statusCode==200){
      final data = jsonDecode(response.body);
      if(data['val'] == 1){
      }else{
      }
    }
  }


  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed:() => Navigator.of(context, rootNavigator: true).pop('dialog'),
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed:  () {
        setState(() {

          _approve();
          // Navigator.of(context, rootNavigator: true).pop('dialog');
        });

      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("APPROVE"),
      content: Text("Are you sure you want approve?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  //
  //
  // showAlertDialog1(BuildContext context) {
  //   // set up the buttons
  //   Widget cancelButton = FlatButton(
  //     child: Text("Cancel"),
  //     onPressed:() => Navigator.of(context, rootNavigator: true).pop('dialog'),
  //   );
  //   Widget continueButton = FlatButton(
  //     child: Text("Continue"),
  //     onPressed:  () {
  //       setState(() {
  //         Navigator.of(context, rootNavigator: true).pop('dialog');
  //         _reject();
  //       });
  //
  //     },
  //   );
  //   // set up the AlertDialog
  //   AlertDialog alert = AlertDialog(
  //     title: Text("REJECT"),
  //     content: Text("Are you sure you want reject?"),
  //     actions: [
  //       cancelButton,
  //       continueButton,
  //     ],
  //   );
  //   // show the dialog
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return alert;
  //     },
  //   );
  // }


  String showpdf;
  bool pagingNext = true;


  int num =0;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pdfViewerController = PdfViewerController();
    Controller1 = PdfViewerController();

    if(num==0){
      showpdf=widget.url;
    }

    if(widget.vendor==''){
      pagingNext= false;
    }
  }


  getdata() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username  = prefs.getString('position') ?? '';
      fullname  = prefs.getString('fullname') ?? '';
      position  = prefs.getString('position') ?? '';
      id  = prefs.getString('id') ?? '';
    });

  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.request),
        backgroundColor: Colors.deepOrange,
        actions: [
          OutlinedButton(
            onPressed: () {
              showAlertDialog(context);
            },
            child: Text('Approve',style: TextStyle(color: Colors.black),),
            style: OutlinedButton.styleFrom(
              side: BorderSide(width: 1.0, color: Colors.deepOrange),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(1.0),
              ),
            ),
          ),
          // SizedBox(width: 2,),
          // OutlinedButton(
          //   onPressed: () {
          //     _reject();
          //   },
          //   child: Text('Reject',style: TextStyle(color: Colors.black),),
          //   style: OutlinedButton.styleFrom(
          //     side: BorderSide(width: 1.0, color: Colors.deepOrange),
          //     shape: RoundedRectangleBorder(
          //       borderRa  dius: BorderRadius.circular(1.0),
          //     ),
          //   ),
          // ),
        ],
      ),
      body:SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 10,),
            Visibility(
              visible: pagingNext,
                child:Padding(
                  padding: EdgeInsets.only(right: 20,left: 20),
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.deepOrange
                        ),
                        borderRadius: BorderRadius.circular(4)
                    ),

                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 3, right: 3),
                            child: IconButton(
                                onPressed: (){
                                  setState(() {
                                    showpdf=widget.url;
                                  });
                                },
                                color: Colors.orange,
                                icon: Icon(Icons.arrow_back,color: Colors.deepOrange,

                                )
                            ),
                          ),

                        ),

                        Container(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 3, right: 3),
                            child:IconButton(
                                onPressed: (){
                                  setState(() {
                                    showpdf=widget.urlpo;
                                  });
                                },
                                icon: Icon(Icons.arrow_forward,color: Colors.deepOrange,

                                )
                            ),
                          ),

                        ),

                      ],
                    ),
                  ),
                ),
            ),

            SizedBox(height: 10,),

            Container(
                height: 610.0,
              child: SfPdfViewer.network(showpdf,
                  controller: _pdfViewerController,
                  key: _pdfViewerStateKey)
            ),



          ],
        ),
      )
    );
  }

}