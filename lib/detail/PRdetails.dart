// @dart=2.9
import 'dart:async';
import 'dart:convert';
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:prycegas_approver/Main_Screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;


class PRDetails extends StatefulWidget {

  final String url;
  final String urlpo;
  final String code;
  final String pocode;
  final String request;
  final String vendor;


  PRDetails({ this.url, this.urlpo, this.code, this.pocode,this.request,this.vendor});

  @override
  State<StatefulWidget> createState() {
    return _ViewPageState();
  }
}


class _ViewPageState extends State<PRDetails> {
  PdfViewerController _pdfViewerController;
  final GlobalKey<SfPdfViewerState> _pdfViewerStateKey = GlobalKey();

  PdfViewerController Controller1;
  GlobalKey<SfPdfViewerState> StateKey1 = GlobalKey();
  String datetime = DateTime.now().toString();
  SharedPreferences preferences;
  String username = '';
  String fullname = '';
  String position = '';
  String id = '';
  String showpdf;
  bool pagingNext = true;


  int num =0;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pdfViewerController = PdfViewerController();
    Controller1 = PdfViewerController();

    if(num==0){
      showpdf=widget.url;
    }

    if(widget.vendor==''){
      pagingNext= false;
    }
  }


  getdata() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username  = prefs.getString('position') ?? '';
      fullname  = prefs.getString('fullname') ?? '';
      position  = prefs.getString('position') ?? '';
      id  = prefs.getString('id') ?? '';
    });

  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.request),
          backgroundColor: Colors.deepOrange,

        ),
        body:SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(height: 10,),
              Visibility(
                visible: pagingNext,
                child:Padding(
                  padding: EdgeInsets.only(right: 20,left: 20),
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.deepOrange
                        ),
                        borderRadius: BorderRadius.circular(4)
                    ),

                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 3, right: 3),
                            child: IconButton(
                                onPressed: (){
                                  setState(() {
                                    showpdf=widget.url;
                                  });
                                },
                                color: Colors.orange,
                                icon: Icon(Icons.arrow_back,color: Colors.deepOrange,

                                )
                            ),
                          ),

                        ),

                        Container(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 3, right: 3),
                            child:IconButton(
                                onPressed: (){
                                  setState(() {
                                    showpdf=widget.urlpo;
                                  });
                                },
                                icon: Icon(Icons.arrow_forward,color: Colors.deepOrange,

                                )
                            ),
                          ),

                        ),

                      ],
                    ),
                  ),
                ),
              ),

              SizedBox(height: 10,),

              Container(
                  height: 610.0,
                  child: SfPdfViewer.network(showpdf,
                      controller: _pdfViewerController,
                      key: _pdfViewerStateKey)
              ),



            ],
          ),
        )
    );
  }

}