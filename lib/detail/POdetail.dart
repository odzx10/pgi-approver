import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
class PODetails extends StatefulWidget {

  final String name;
  final String url;

  PODetails({required this.name,required this.url});

  @override
  State<StatefulWidget> createState() {
    return _ViewPageState();
  }
}

class _ViewPageState extends State<PODetails> {
  late PdfViewerController _pdfViewerController;
  final GlobalKey<SfPdfViewerState> _pdfViewerStateKey = GlobalKey();



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pdfViewerController = PdfViewerController();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name),
        backgroundColor: Colors.deepOrange,

      ),
      body: SfPdfViewer.network(widget.url,
          controller: _pdfViewerController,
          key: _pdfViewerStateKey),
    );
  }

}