
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:prycegas_approver/Main_Screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
class ViewPage extends StatefulWidget {

  final String name;
  final String url;
  final String code;
  final String date;
  final String liter;
  final String fuel;
  final String description;
  final String asset;
  final String plate;
  final String reading;
  final String remark;
  final String approvedby;
  final String status;
  final String center;

  ViewPage({required this.name,required this.url,required this.code,required this.date,required this.liter,required this.fuel,required this.description,required this.asset,required this.plate,required this.reading,required this.remark,required this.approvedby,required this.status,required this.center});

  @override
  State<StatefulWidget> createState() {
    return _ViewPageState();
  }
}

class _ViewPageState extends State<ViewPage> {
  late PdfViewerController _pdfViewerController;
  final GlobalKey<SfPdfViewerState> _pdfViewerStateKey = GlobalKey();

 SharedPreferences? preferences;
 String username = '';
 String fullname = '';
 String position = '';
 String id = '';


 Future _approve() async {
   final response = await http.post(Uri.parse("http://prycegas.net/pomobile/approved.php"),body:{
     "code":widget.code,
     "url": widget.url,
     "date": widget.date,
     "liter": widget.liter,
     "fuel": widget.fuel,
     "description": widget.description,
     "asset": widget.asset,
     "plate": widget.plate,
     "reading": widget.reading,
     "remark": widget.remark,
     "requestby": widget.name,
     "approvedby": widget.approvedby,
     "status": "Approved",
     "center": widget.center,
     "id": id
   });
   Navigator.push(context, MaterialPageRoute(builder: (context)=>MainPage()),);
   if(response.statusCode==200){
     final data = jsonDecode(response.body);
     if(data['val'] == 1){
     }else{
     }
   }
 }

 Future _reject() async {
   final response = await http.post(Uri.parse("http://prycegas.net/pomobile/reject.php"),body:{
     "code":widget.code,
     "status": "Rejected",
   });
   Navigator.push(context, MaterialPageRoute(builder: (context)=>MainPage()),);
   if(response.statusCode==200){
     final data = jsonDecode(response.body);
     if(data['val'] == 1){
     }else{
     }
   }
 }


 showAlertDialog(BuildContext context) {
   // set up the buttons
   Widget cancelButton = FlatButton(
     child: Text("Cancel"),
     onPressed:() => Navigator.of(context, rootNavigator: true).pop('dialog'),
   );
   Widget continueButton = FlatButton(
     child: Text("Continue"),
     onPressed:  () {
       setState(() {
         Navigator.of(context, rootNavigator: true).pop('dialog');
         _approve();
       });

     },
   );
   // set up the AlertDialog
   AlertDialog alert = AlertDialog(
     title: Text("APPROVE"),
     content: Text("Are you sure you want approve?"),
     actions: [
       cancelButton,
       continueButton,
     ],
   );
   // show the dialog
   showDialog(
     context: context,
     builder: (BuildContext context) {
       return alert;
     },
   );
 }


 showAlertDialog1(BuildContext context) {
   // set up the buttons
   Widget cancelButton = FlatButton(
     child: Text("Cancel"),
     onPressed:() => Navigator.of(context, rootNavigator: true).pop('dialog'),
   );
   Widget continueButton = FlatButton(
     child: Text("Continue"),
     onPressed:  () {
       setState(() {
         Navigator.of(context, rootNavigator: true).pop('dialog');
         _reject();
       });

     },
   );
   // set up the AlertDialog
   AlertDialog alert = AlertDialog(
     title: Text("REJECT"),
     content: Text("Are you sure you want reject?"),
     actions: [
       cancelButton,
       continueButton,
     ],
   );
   // show the dialog
   showDialog(
     context: context,
     builder: (BuildContext context) {
       return alert;
     },
   );
 }


    @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pdfViewerController = PdfViewerController();
  }



 getdata() async{
   SharedPreferences prefs = await SharedPreferences.getInstance();
   setState(() {
     username  = prefs.getString('position') ?? '';
     fullname  = prefs.getString('fullname') ?? '';
     position  = prefs.getString('position') ?? '';
     id  = prefs.getString('id') ?? '';
   });

 }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
        title: Text(widget.name),
          backgroundColor: Colors.deepOrange,
          actions: [
            OutlinedButton(
              onPressed: () {
                showAlertDialog(context);
              },
              child: Text('Approve',style: TextStyle(color: Colors.black),),
              style: OutlinedButton.styleFrom(
                side: BorderSide(width: 1.0, color: Colors.deepOrange),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(1.0),
                ),
              ),
            ),
            SizedBox(width: 2,),
            OutlinedButton(
              onPressed: () {
                showAlertDialog1(context);
              },
              child: Text('Reject',style: TextStyle(color: Colors.black),),
              style: OutlinedButton.styleFrom(
                side: BorderSide(width: 1.0, color: Colors.deepOrange),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(1.0),
                ),
              ),
            ),
          ],
    ),
    body: SfPdfViewer.network(widget.url,
        controller: _pdfViewerController,
        key: _pdfViewerStateKey),
    );
  }

}