import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:prycegas_approver/Main_Screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
class CAViewPage extends StatefulWidget {

  final String requesting;
  final String url;
  final String position;
  final String assign_area;
  final String purpose;
  final String recommended;
  final String approved;
  final String amount;
  final String date;


  CAViewPage({required this.requesting,required this.url,required this.position,required this.assign_area,required this.purpose,required this.recommended,required this.approved,required this.amount,required this.date});

  @override
  State<StatefulWidget> createState() {
    return _ViewPageState();
  }
}

class _ViewPageState extends State<CAViewPage> {
  late PdfViewerController _pdfViewerController;
  final GlobalKey<SfPdfViewerState> _pdfViewerStateKey = GlobalKey();

  SharedPreferences? preferences;
  String username = '';
  String fullname = '';
  String position = '';
  String id = '';


  Future _approve() async {
    final response = await http.post(Uri.parse("http://prycegas.net/pomobile/caapproved.php"),body:{
      "name":widget.requesting,
      "url": widget.url,
      "position": widget.position,
      "assign_area": widget.assign_area,
      "purpose": widget.purpose,
      "recommended": widget.recommended,
      "approved": widget.approved,
      "amount": widget.amount,
      "date": widget.date
    });
    Navigator.push(context, MaterialPageRoute(builder: (context)=>MainPage()),);
    if(response.statusCode==200){
      final data = jsonDecode(response.body);
      if(data['val'] == 1){
      }else{
      }
    }
  }

  Future _reject() async {
    final response = await http.post(Uri.parse("http://prycegas.net/pomobile/careject.php"),body:{
      "date":widget.date,
      "status": "Rejected",
    });
    Navigator.push(context, MaterialPageRoute(builder: (context)=>MainPage()),);
    if(response.statusCode==200){
      final data = jsonDecode(response.body);
      if(data['val'] == 1){
      }else{
      }
    }
  }


  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed:() => Navigator.of(context, rootNavigator: true).pop('dialog'),
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed:  () {
        setState(() {
          Navigator.of(context, rootNavigator: true).pop('dialog');
          _approve();
        });

      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("APPROVE"),
      content: Text("Are you sure you want approve?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  showAlertDialog1(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed:() => Navigator.of(context, rootNavigator: true).pop('dialog'),
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed:  () {
        setState(() {
          Navigator.of(context, rootNavigator: true).pop('dialog');
          _reject();
        });

      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("REJECT"),
      content: Text("Are you sure you want reject?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pdfViewerController = PdfViewerController();
  }



  getdata() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username  = prefs.getString('position') ?? '';
      fullname  = prefs.getString('fullname') ?? '';
      position  = prefs.getString('position') ?? '';
      id  = prefs.getString('id') ?? '';
    });

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.requesting),
        backgroundColor: Colors.deepOrange,
        actions: [
          OutlinedButton(
            onPressed: () {
              showAlertDialog(context);
            },
            child: Text('Approve',style: TextStyle(color: Colors.black),),
            style: OutlinedButton.styleFrom(
              side: BorderSide(width: 1.0, color: Colors.deepOrange),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(1.0),
              ),
            ),
          ),
          SizedBox(width: 2,),
          OutlinedButton(
            onPressed: () {
              showAlertDialog1(context);
            },
            child: Text('Reject',style: TextStyle(color: Colors.black),),
            style: OutlinedButton.styleFrom(
              side: BorderSide(width: 1.0, color: Colors.deepOrange),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(1.0),
              ),
            ),
          ),
        ],
      ),
      body: SfPdfViewer.network(widget.url,
          controller: _pdfViewerController,
          key: _pdfViewerStateKey),
    );
  }

}