import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:prycegas_approver/detail/POdetail.dart';
import 'package:prycegas_approver/detail/PRdetails.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'CAdetail.dart';

class PRPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<PRPage> {
  SharedPreferences? preferences;
  String username = '';
  String fullname = '';
  String position = '';

  Future<List<dynamic>> allOrder() async{
    var url ="http://prycegas.net/pomobile/prshowapproved.php";
    var response = await http.post(Uri.parse(url),body:{'name':fullname});
    return json.decode(response.body);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getdata();
  }

  getdata() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username  = prefs.getString('position') ?? '';
      fullname  = prefs.getString('fullname') ?? '';
      position  = prefs.getString('position') ?? '';

    });

  }
  @override
  Widget build(BuildContext context) => WillPopScope(
    child: Scaffold(
      // endDrawer: NavigationDrawerWidget(),
      appBar: AppBar(
        title: Text("PR Approved"),
        backgroundColor: Colors.deepOrange,

      ),

      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[

          Expanded(
            child:FutureBuilder<List<dynamic>>(
              future:allOrder(),
              builder: (context, snapshot){
                if(snapshot.hasError) print(snapshot.error);
                return snapshot.hasData ? ListView.builder(
                    itemCount: snapshot.requireData.length,
                    itemBuilder: (context,currentIndex){
                      List<dynamic> list = snapshot.requireData;
                      return new GestureDetector(
                        onTap: ()=>Navigator.of(context).push(
                          new MaterialPageRoute(
                              builder: (BuildContext context)=> new PRDetails(
                                url:"http://prycegas.net/pomobile/"+list[currentIndex]['url'],
                                urlpo:"http://prycegas.net/pomobile/"+list[currentIndex]['urlpo'],
                                code:list[currentIndex]['code'],
                                pocode:list[currentIndex]['pocode'],
                                request:list[currentIndex]['request'],
                                vendor:list[currentIndex]['vendorName'],
                              )
                          ),
                        ),
                        child: new Card(
                          child: new ListTile(
                            title: new Text(list[currentIndex]['request']),
                            leading: new Icon(Icons.picture_as_pdf),
                            subtitle: new Text(list[currentIndex]['dateCreated']),
                            trailing: Text("00"+list[currentIndex]['code']),
                          ),
                        ),
                      );

                    }):Center();

              },
            ),
          ),

        ],
      ),

    ),
    onWillPop: () async => true,
  );

}