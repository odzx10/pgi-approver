// @dart=2.9
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:prycegas_approver/Main_Screen.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  Map user1;
  TextEditingController user = TextEditingController();
  TextEditingController pass = TextEditingController();
  SharedPreferences preferences;
  bool _passwordVisible = false;

  void login1() async{
    final response = await http.post(Uri.parse('http://prycegas.net/pomobile/login.php'),body:{
      "user": user.text,
      "pass": pass.text,
    });

    if(response.statusCode==200){
      final data = jsonDecode(response.body);
      if(data['val']==1){
        print(data['info']);
        user1 = data['info'];
        initializePreference();
        Navigator.push(context, MaterialPageRoute(builder: (context)=>MainPage()),);
      }else{
        print("bad breath");
      }

    }

  }


  Future<void> initializePreference() async{
    this.preferences = await SharedPreferences.getInstance();
    this.preferences?.setString("id", user1['id']);
    this.preferences?.setString("user", user1['username']);
    this.preferences?.setString("fullname", user1['fullname']);
    this.preferences?.setString("position", user1['position']);

  }

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    // autoLogIn();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(child: Column(
                children: <Widget>[
                  SizedBox(height: 80,),
                  Container(
                    padding: EdgeInsets.only(top: 200),
                    height: 100,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/pgi logo.png"),
                          fit: BoxFit.fitHeight
                      ),

                    ),
                  ),
                  SizedBox(height: 30,),

                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      children: <Widget>[
                        TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                            ),
                            hintText: 'Username',
                          ),
                          controller: user,
                        ),

                        SizedBox(height: 5.0,),

                        TextFormField(
                          keyboardType: TextInputType.text,
                          controller: pass,
                          obscureText: !_passwordVisible,//This will obscure text dynamically
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Password',
                            hintText: 'Enter your password',
                            // Here is key idea
                            suffixIcon: IconButton(
                              icon: Icon(
                                // Based on passwordVisible state choose the icon
                                _passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Theme.of(context).primaryColorDark,
                              ),
                              onPressed: () {
                                // Update the state i.e. toogle the state of passwordVisible variable
                                setState(() {
                                  _passwordVisible = !_passwordVisible;
                                });
                              },
                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        MaterialButton(
                          minWidth: double.infinity,
                          height: 60,
                          onPressed: login1,
                          color: Color(0xffF78504),
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),

                          ),
                          child: Text(
                            "Login", style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            color: Colors.white,

                          ),
                          ),

                        ),


                      ],
                    ),
                  ),




                ],
              ))
            ],
          ),
        ),
      ),
      onWillPop: () async => false,
    );
  }
}

// we will be creating a widget for text field
Widget inputFile({label, obscureText = false})
{
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        label,
        style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w400,
            color:Colors.black87
        ),

      ),
      SizedBox(
        height: 5,
      ),
      TextField(
        obscureText: obscureText,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 0,
                horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.grey
              ),

            ),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey)
            )
        ),
      ),
      SizedBox(height: 10,)
    ],
  );
}