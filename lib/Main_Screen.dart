
import 'package:flutter/material.dart';
import 'package:prycegas_approver/pages/canotif.dart';
import 'package:prycegas_approver/pages/ponotif.dart';
import 'package:prycegas_approver/pages/prnotif.dart';
import 'package:prycegas_approver/widget/navigation_drawer_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';


class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with SingleTickerProviderStateMixin{
  SharedPreferences? preferences;
  String username = '';
  String fullname = '';
  String position = '';

  getdata() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username  = prefs.getString('position') ?? '';
      fullname  = prefs.getString('fullname') ?? '';
      position  = prefs.getString('position') ?? '';

    });

  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getdata();
  }
  @override
  Widget build(BuildContext context) => WillPopScope(
    child: DefaultTabController(
      length:2,
      child: Scaffold(
        drawer: NavigationDrawerWidget(),
        appBar: AppBar(
          title: Text(fullname),
          backgroundColor: Colors.white,
          bottom: TabBar(
            isScrollable: true,
            indicatorPadding: const EdgeInsets.only(left: 10),
            indicatorWeight: 3.0,
            indicatorColor:Colors.black,
            labelColor: Colors.black,
            unselectedLabelColor: Colors.deepOrange,
            tabs: <Widget>[
              Tab(
                child:Text('GAS PO'),
              ),
              // Tab(
              //   child:Text('CASH ADVANCE'),
              // ),
              Tab(
                child:Text('PURCHASE REQUEST'),
              ),
            ],
          ),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            PONotification(),
            // CANotification(),
            PRNotification(),
          ],
        ),
      ),
    ),
    onWillPop: () async => false,
  );
}



