import 'dart:async';
import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:prycegas_approver/detail/Viewpdf.dart';
import 'package:prycegas_approver/detail/prviewpdf.dart';
import 'package:prycegas_approver/widget/navigation_drawer_widget.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class PRNotification extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<PRNotification> {
  SharedPreferences? preferences;
  String username = '';
  String fullname = '';
  String position = '';
  late Timer timer;
  late Timer timer1;
  late StreamSubscription iosSubscription;
  FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
  String id= '';
  var url;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => getdata());
  }
  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }


  getdata() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username  = prefs.getString('position') ?? '';
      fullname  = prefs.getString('fullname') ?? '';
      position  = prefs.getString('position') ?? '';
      id  = prefs.getString('id') ?? '';
    });
    if(id=='1'){
        url ="http://prycegas.net/pomobile/prfile1.php";
    }else if(id=="2"){
        url ="http://prycegas.net/pomobile/prfile2.php";
    }else if(id=="3"){
        url ="http://prycegas.net/pomobile/prfile3.php";
    }else if(id=="4"){
        url ="http://prycegas.net/pomobile/prfile4.php";
    }else if(id=="5"){
        url ="http://prycegas.net/pomobile/prfile5.php";
    }
  }

  Future<List<dynamic>> allOrder() async{
    var response = await http.post(Uri.parse(url),body:{'name':fullname});
    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
    child: Scaffold(


      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[

          Expanded(
            child:FutureBuilder<List<dynamic>>(
              future:allOrder(),
              builder: (context, snapshot){
                if(snapshot.hasError) print(snapshot.error);
                return snapshot.hasData ? ListView.builder(
                    itemCount: snapshot.requireData.length,
                    itemBuilder: (context,currentIndex){
                      List<dynamic> list = snapshot.requireData;
                      return new GestureDetector(
                        onTap: ()=>Navigator.of(context).push(
                          new MaterialPageRoute(
                              builder: (BuildContext context)=> new PRViewPage(
                                url:"http://prycegas.net/pomobile/"+list[currentIndex]['url'],
                                urlpo:"http://prycegas.net/pomobile/"+list[currentIndex]['urlpo'],
                                code:list[currentIndex]['code'],
                                pocode:list[currentIndex]['pocode'],
                                request:list[currentIndex]['request'],
                                recommended:list[currentIndex]['recommend'],
                                dept:list[currentIndex]['dept'],
                                dateCreated:list[currentIndex]['dateCreated'],
                                vendor:list[currentIndex]['vendorName'],
                                purpose:list[currentIndex]['purpose'],
                                remarks:list[currentIndex]['remarks'],
                                item1:list[currentIndex]['item1'],
                                item2:list[currentIndex]['item2'],
                                item3:list[currentIndex]['item3'],
                                item4:list[currentIndex]['item4'],
                                item5:list[currentIndex]['item5'],
                                item6:list[currentIndex]['item6'],
                                item7:list[currentIndex]['item7'],
                                item8:list[currentIndex]['item8'],
                                item9:list[currentIndex]['item9'],
                                item10:list[currentIndex]['item10'],
                                item11:list[currentIndex]['item11'],
                                item12:list[currentIndex]['item12'],
                                item13:list[currentIndex]['item13'],
                                item14:list[currentIndex]['item14'],
                                item15:list[currentIndex]['item15'],
                                item16:list[currentIndex]['item16'],
                                item17:list[currentIndex]['item17'],
                                item18:list[currentIndex]['item18'],
                                item19:list[currentIndex]['item19'],
                                item20:list[currentIndex]['item20'],
                                item21:list[currentIndex]['item21'],
                                item22:list[currentIndex]['item22'],
                                item23:list[currentIndex]['item23'],
                                item24:list[currentIndex]['item24'],
                                item25:list[currentIndex]['item25'],
                                item26:list[currentIndex]['item26'],
                                item27:list[currentIndex]['item27'],
                                item28:list[currentIndex]['item28'],
                                item29:list[currentIndex]['item29'],
                                item30:list[currentIndex]['item30'],
                                item31:list[currentIndex]['item31'],
                                item32:list[currentIndex]['item32'],
                                item33:list[currentIndex]['item33'],
                                item34:list[currentIndex]['item34'],
                                item35:list[currentIndex]['item35'],
                                item36:list[currentIndex]['item36'],
                                item37:list[currentIndex]['item37'],
                                item38:list[currentIndex]['item38'],
                                item39:list[currentIndex]['item39'],
                                item40:list[currentIndex]['item40'],


                              )
                          ),
                        ),
                        child: new Card(
                          child: new ListTile(
                            title: new Text(list[currentIndex]['request']),
                            leading: new Icon(Icons.picture_as_pdf),
                            subtitle: new Text(list[currentIndex]['dateCreated']),
                            trailing: Text("00"+list[currentIndex]['code']),
                          ),
                        ),
                      );

                    }):Center();



              },
            ),
          ),

        ],
      ),

    ),
    onWillPop: () async => false,
  );

}