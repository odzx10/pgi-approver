import 'dart:async';
import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:prycegas_approver/detail/Viewpdf.dart';
import 'package:prycegas_approver/detail/caviewpdf.dart';
import 'package:prycegas_approver/widget/navigation_drawer_widget.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class CANotification extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<CANotification> {
  SharedPreferences? preferences;
  String username = '';
  String fullname = '';
  String position = '';
  String id= '';
  late Timer timer;
  late Timer timer1;
  late StreamSubscription iosSubscription;
  FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;





  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    timer = Timer.periodic(Duration(seconds: 5), (Timer t) => getdata());

  }

  getdata() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username  = prefs.getString('position') ?? '';
      fullname  = prefs.getString('fullname') ?? '';
      position  = prefs.getString('position') ?? '';
      id  = prefs.getString('id') ?? '';
    });



  }
  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }


  Future<List<dynamic>> allOrder() async{
    var url ="http://prycegas.net/pomobile/cafile.php";
    var response = await http.post(Uri.parse(url),body:{'name':fullname});
    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
    child: Scaffold(

      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[

          Expanded(
            child:FutureBuilder<List<dynamic>>(
              future:allOrder(),
              builder: (context, snapshot){
                if(snapshot.hasError) print(snapshot.error);
                return snapshot.hasData ? ListView.builder(
                    itemCount: snapshot.requireData.length,
                    itemBuilder: (context,currentIndex){
                      List<dynamic> list = snapshot.requireData;
                      return new GestureDetector(
                        onTap: ()=>Navigator.of(context).push(
                          new MaterialPageRoute(
                              builder: (BuildContext context)=> new CAViewPage(

                                url:"http://prycegas.net/pomobile/"+list[currentIndex]['url'],
                                requesting:list[currentIndex]['requesting'],
                                position:list[currentIndex]['position'],
                                assign_area:list[currentIndex]['assign_area'],
                                purpose:list[currentIndex]['purpose'],
                                recommended:list[currentIndex]['recommended'],
                                approved:list[currentIndex]['approved'],
                                amount:list[currentIndex]['amount'],
                                date:list[currentIndex]['date'],


                              )
                          ),
                        ),
                        child: new Card(
                          child: new ListTile(
                            title: new Text(list[currentIndex]['requesting']+ " - "+ list[currentIndex]['position']),
                            leading: new Icon(Icons.picture_as_pdf),
                            subtitle: new Text(list[currentIndex]['monthUp']+"/"+list[currentIndex]['dayUp']+"/"+list[currentIndex]['yearUp']),
                            trailing: Text("Amount: "+ list[currentIndex]['amount']+" Pesos"),
                          ),
                        ),
                      );

                    }):Center();



              },
            ),
          ),

        ],
      ),

    ),
    onWillPop: () async => false,
  );
}