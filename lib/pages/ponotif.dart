import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:prycegas_approver/detail/Viewpdf.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class PONotification extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<PONotification> {
  SharedPreferences? preferences;
  String username = '';
  String fullname = '';
  String position = '';
  late Timer timer;
  late Timer timer1;
  late StreamSubscription iosSubscription;
  FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;





  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getdata();
    timer = Timer.periodic(Duration(seconds: 1), (Timer t) => getdata());
    registerNotification();
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  getdata() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      username  = prefs.getString('position') ?? '';
      fullname  = prefs.getString('fullname') ?? '';
      position  = prefs.getString('position') ?? '';

    });

  }

  void registerNotification() async {

    SharedPreferences prefs =  await SharedPreferences.getInstance();
    setState(() {
      fullname = prefs.getString('fullname') ?? '';
      print(fullname);
    });
    firebaseMessaging.getToken().then((token) async {
      print('token: $token');
      FirebaseFirestore.instance.collection('users').doc(fullname).update({'pushToken': token});

      final response = await http.post(Uri.parse('http://prycegas.net/pomobile/updatetoken.php'),body:{"name":fullname,"token":token});
      final data = jsonDecode(response.body);
      int value = data['value'];
      String message = data['message'];
      if(value == 1 ){
        setState(() {

        });
      }else{
        print(message);
      }

    }).catchError((err) {
      Fluttertoast.showToast(msg: err.message.toString());
    });
  }


  Future<List<dynamic>> allOrder() async{
    var url ="http://prycegas.net/pomobile/pofile.php";
    var response = await http.post(Uri.parse(url),body:{'name':fullname});
    return json.decode(response.body);
  }

  @override
  Widget build(BuildContext context) => WillPopScope(
    child: Scaffold(

      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child:FutureBuilder<List<dynamic>>(
              future:allOrder(),
              builder: (context, snapshot){
                if(snapshot.hasError) print(snapshot.error);
                return snapshot.hasData ? ListView.builder(
                    itemCount: snapshot.requireData.length,
                    itemBuilder: (context,currentIndex){
                      List<dynamic> list = snapshot.requireData;
                      return new GestureDetector(
                        onTap: ()=>Navigator.of(context).push(
                          new MaterialPageRoute(
                              builder: (BuildContext context)=> new ViewPage(
                                name: list[currentIndex]['requestby'],
                                url:"http://prycegas.net/pomobile/"+list[currentIndex]['url'],
                                code:list[currentIndex]['code'],
                                date:list[currentIndex]['date'],
                                liter:list[currentIndex]['liter'],
                                fuel:list[currentIndex]['fuel'],
                                description:list[currentIndex]['description'],
                                asset:list[currentIndex]['asset'],
                                plate:list[currentIndex]['plate'],
                                reading:list[currentIndex]['reading'],
                                remark:list[currentIndex]['remark'],
                                approvedby:list[currentIndex]['approvedby'],
                                status:list[currentIndex]['status'],
                                center:list[currentIndex]['center'],
                              )
                          ),
                        ),
                        child: new Card(
                          child: new ListTile(
                            title: new Text(list[currentIndex]['requestby']+ " - "+ list[currentIndex]['plate']),
                            leading: new Icon(Icons.picture_as_pdf),
                            subtitle: new Text(list[currentIndex]['monthUp']+"/"+list[currentIndex]['dayUp']+"/"+list[currentIndex]['yearUp']),
                            trailing: Text("00"+list[currentIndex]['code']),
                          ),
                        ),
                      );

                    }):Center();



              },
            ),
          ),

        ],
      ),

    ),
    onWillPop: () async => false,
  );
}